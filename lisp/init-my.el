;; 自定义配置

;; 设置字体
;; (set-frame-font "-outline-微软雅黑-normal-normal-normal-sans-17-*-*-*-p-*-iso8859-1")
;; (set-frame-font "-outline-Courier New-normal-normal-normal-mono-16-*-*-*-c-*-iso8859-1")

;; 用下面的字体，需要先下载并安装Source Code Pro字体。https://www.fonts.net.cn/font-search-result.html?q=Source+Code+Pro
;; 这个字体实现orgmode中table中英文对齐。support目录下已经下载好了。
(set-face-attribute 'default nil :font
                    (format   "%s:pixelsize=%d" "Source Code Pro" 17))
(dolist (charset '(kana han cjk-misc bopomofo))
  (set-fontset-font (frame-parameter nil 'font) charset
                    (font-spec :family "微软雅黑" :size 20)))

(set-background-color "#222") ;; 使用黑色背景
(set-foreground-color "#eee") ;; 使用白色前景
(set-face-foreground 'region "green")  ;; 区域前景颜色设为绿色
(set-face-background 'region "blue") ;; 区域背景色设为蓝色

;; org 自动换行
(add-hook 'org-mode-hook (lambda () (setq toggle-truncate-lines t)))



;; https://stackoverflow.com/questions/8837712/emacs-creates-buffers-very-slowly
;; 解决打开文件慢的问题
(remove-hook 'find-file-hooks 'vc-refresh-state)

;; 桌面环境恢复
(desktop-save-mode 1)

;; 启动时窗口最大化
;; (add-to-list 'default-frame-alist '(fullscreen . maximized))

(electric-pair-mode t)                       ; 自动补全括号
(add-hook 'prog-mode-hook #'show-paren-mode) ; 编程模式下，光标在括号上时高亮另一个括号
(column-number-mode t)                       ; 在 Mode line 上显示列号
;; (use-package linum
;;   :init
;;   (progn
;;     (global-linum-mode t)
;;     (setq linum-format "%4d  ")
;;       (set-face-background 'linum nil)
;;       ))

(global-auto-revert-mode t)                  ; 当另一程序修改了文件时，让 Emacs 及时刷新 Buffer
(delete-selection-mode t)                    ; 选中文本后输入文本会替换文本（更符合我们习惯了的其它编辑器的逻辑）
(setq inhibit-startup-message t)             ; 关闭启动 Emacs 时的欢迎界面
(setq make-backup-files nil)                 ; 关闭文件自动备份
(add-hook 'prog-mode-hook #'hs-minor-mode)   ; 编程模式下，可以折叠代码块
;; (global-display-line-numbers-mode 1)         ; 在 Window 显示行号

;; tab改为4个空格
(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)
;; (setq indent-line-function 'insert-tab)

(setq create-lockfiles nil)

(tool-bar-mode -1)                           ; 关闭 Tool bar
(menu-bar-mode -1)
(when (display-graphic-p) (toggle-scroll-bar -1)) ; 图形界面时关闭滚动条

(savehist-mode 1)                            ; （可选）打开 Buffer 历史记录保存
;; (setq display-line-numbers-type 'relative)   ; （可选）显示相对行号
;; (add-to-list 'default-frame-alist '(width . 90))  ; （可选）设定启动图形界面时的初始 Frame 宽度（字符数）
;; (add-to-list 'default-frame-alist '(height . 55)) ; （可选）设定启动图形界面时的初始 Frame 高度（字符数）


;; 自定义两个函数
;; Faster move cursor
(defun next-ten-lines()
  "Move cursor to next 10 lines."
  (interactive)
  (next-line 10))

(defun previous-ten-lines()
  "Move cursor to previous 10 lines."
  (interactive)
  (previous-line 10))
;;绑定到快捷键
(global-set-key (kbd "M-n") 'next-ten-lines)            ; 光标向下移动 10 行
(global-set-key (kbd "M-p") 'previous-ten-lines)        ; 光标向上移动 10 行

;; C-j 被后面的插件覆盖了 
;; (global-set-key (kbd "C-j") nil)
;; 删去光标所在行（在图形界面时可以用 "C-S-<DEL>"，终端常会拦截这个按法)
;; (global-set-key (kbd "C-j C-k") 'kill-whole-line)


;; 交换上下行begin
;move line up down
(defun move-text-internal (arg)
  (cond
   ((and mark-active transient-mark-mode)
    (if (> (point) (mark))
        (exchange-point-and-mark))
    (let ((column (current-column))
          (text (delete-and-extract-region (point) (mark))))
      (forward-line arg)
      (move-to-column column t)
      (set-mark (point))
      (insert text)
      (exchange-point-and-mark)
      (setq deactivate-mark nil)))
   (t
    (let ((column (current-column)))
      (beginning-of-line)
      (when (or (> arg 0) (not (bobp)))
        (forward-line)
        (when (or (< arg 0) (not (eobp)))
          (transpose-lines arg))
        (forward-line -1))
      (move-to-column column t)))))

(defun move-text-down (arg)
  "Move region (transient-mark-mode active) or current line
  arg lines down."
  (interactive "*p")
  (move-text-internal arg))

(defun move-text-up (arg)
  "Move region (transient-mark-mode active) or current line
  arg lines up."
  (interactive "*p")
  (move-text-internal (- arg)))

(global-set-key [S-C-up] 'move-text-up)
(global-set-key [S-C-down] 'move-text-down)
;; 上下行交换end

;; 用y/n代替yes/no
(defalias 'yes-or-no-p 'y-or-n-p)

;; 设置系统编码
(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(setq default-buffer-file-coding-system 'utf-8)

;; 禁用提示音
(setq visible-bell 0)


(provide 'init-my)
