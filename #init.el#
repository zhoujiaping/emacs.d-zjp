;; 从零开始配置emacs
;; --by stan


;; emacs 单例
(server-start)

;; 设置模块加载路径
(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))

;; 操作系统判断
(defconst *is-mac* (eq system-type 'darwin))
(defconst *is-linux* (eq system-type 'gnu/linux))
(defconst *is-windows* (or (eq system-type 'ms-dos) (eq system-type 'windows-nt)))

;; 调整gc参数，加速启动
(let ((init-gc-cons-threshold (* 128 1024 1024)))
  (setq gc-cons-threshold init-gc-cons-threshold)
  (add-hook 'emacs-startup-hook
            (lambda () (setq gc-cons-threshold (* 20 1024 1024)))))

(setq custom-file (locate-user-emacs-file "custom.el"))

;; 设置仓库镜像
(setq package-archives '(("gnu"   . "http://mirrors.tuna.tsinghua.edu.cn/elpa/gnu/")
                         ("melpa" . "http://mirrors.tuna.tsinghua.edu.cn/elpa/melpa/")))

;; https://segmentfault.com/a/1190000039902535
(eval-when-compile
  (require 'use-package))

;; 性能统计
(use-package init-benchmarking)
;; (require 'init-benchmarking) ;; Measure startup time


;; 自定义配置
(use-package init-my)

;; 自动补全模式
(add-hook 'after-init-hook 'global-company-mode)

;; 彩虹括号
(use-package rainbow-delimiters
;;  :disabled t
  :ensure t
  :config
  (add-hook 'prog-mode-hook 'rainbow-delimiters-mode)
  (custom-set-faces
   ;; custom-set-faces was added by Custom.
   ;; If you edit it by hand, you could mess it up, so be careful.
   ;; Your init file should contain only one such instance.
   ;; If there is more than one, they won't work right.
   '(rainbow-delimiters-depth-1-face ((t (:foreground "dark orange"))))
   '(rainbow-delimiters-depth-2-face ((t (:foreground "deep pink"))))
   '(rainbow-delimiters-depth-3-face ((t (:foreground "chartreuse"))))
   '(rainbow-delimiters-depth-4-face ((t (:foreground "deep sky blue"))))
   '(rainbow-delimiters-depth-5-face ((t (:foreground "yellow"))))
   '(rainbow-delimiters-depth-6-face ((t (:foreground "orchid"))))
   '(rainbow-delimiters-depth-7-face ((t (:foreground "spring green"))))
   '(rainbow-delimiters-depth-8-face ((t (:foreground "sienna1")))))
)

;; 设置行号
(use-package linum
  :init
  (progn
    (global-linum-mode t)
    (setq linum-format "%4d  ")
      (set-face-background 'linum nil)
    ))


;; 目录操作
(use-package diredfl
  :ensure t
  :config (global-set-key (kbd "C-x C-j") 'dired-jump)            ; dired
)


(use-package which-key
  :ensure t
  :defer nil
  :config (which-key-mode)
  )

(use-package ivy
  :ensure t
  )
(use-package counsel
  :ensure t
  )

;;
;; https://gitee.com/mirrors/emacs-neotree
(use-package neotree
;;  :disabled t
  :defer 1
  :ensure t
  :config
  (global-set-key [f8] 'neotree-toggle)
  (setq neo-theme (if (display-graphic-p)  'arrow))
  ;; (setq neo-theme (if (display-graphic-p) 'icons 'arrow))
  )

(use-package json-mode
  :defer 1
  ;; :disabled t
  )

;; 跳转到行
;; https://juejin.cn/post/6882192486119243789
(use-package avy
  ;;  :disabled t
  :defer 1
  :ensure t
  :config
  (global-set-key (kbd "M-g f") 'avy-goto-line)
)

(use-package csv-mode)

(use-package ace-window
  :config
  (global-set-key (kbd "C-x o") 'ace-window)
)
;; 窗口恢复
(use-package winner-mode
  :ensure nil
  :hook (after-init . winner-mode))
;; 高亮当前行
(use-package hl-line
  :defer 1
  :ensure nil
  :hook (after-init . global-hl-line-mode))
;; 显示，隐藏结构化数据
(use-package hideshow
  :defer 1
  :ensure nil
  :diminish hs-minor-mode
  :bind (:map prog-mode-map
         ("C-c TAB" . hs-toggle-hiding)
         ("M-+" . hs-show-all))
  :hook (prog-mode . hs-minor-mode)
  :custom
  (hs-special-modes-alist
   (mapcar 'purecopy
           '((c-mode "{" "}" "/[*/]" nil nil)
             (c++-mode "{" "}" "/[*/]" nil nil)
             (rust-mode "{" "}" "/[*/]" nil nil)))))
;; 显示空白符
(use-package whitespace
  :defer 1
  :ensure nil
  :hook ((prog-mode markdown-mode conf-mode) . whitespace-mode)
  :config
  (setq whitespace-style '(face trailing)))
;; 文件过大时自动关闭某些插件
(use-package so-long
  :defer 1
  :ensure nil
  :config (global-so-long-mode 1))
;; 外部修改文件时，自动刷新buffer
(use-package autorevert
  :defer 1
  :ensure nil
  :hook (after-init . global-auto-revert-mode))
;; 搜索时显示匹配的个数
(setq isearch-lazy-count t
      lazy-count-prefix-format "%s/%s ")

;; 强化搜索
(use-package ivy
  :defer 1
  :demand
  :hook (after-init . ivy-mode)
  :config
  (ivy-mode 1)
  (setq
        ivy-use-virtual-buffers t
        ivy-initial-inputs-alist nil
        ivy-count-format "%d/%d "
        enable-recursive-minibuffers t
        ivy-re-builders-alist '((t . ivy--regex-ignore-order)))
  (ivy-posframe-mode 0))

(use-package counsel
  :after (ivy)
  :bind (("M-x" . counsel-M-x)
         ("C-x C-f" . counsel-find-file)
         ("C-c f" . counsel-recentf)
         ("C-c g" . counsel-git)))

(use-package swiper
  :after ivy
  :bind (("C-s" . swiper)
         ("C-r" . swiper-isearch-backward))
  :config (setq swiper-action-recenter t
                swiper-include-line-number-in-search t))

;; active Org-babel languages
(org-babel-do-load-languages
    'org-babel-load-languages
      '(;; other Babel languages
           (plantuml . t)))

;; 需要先下载plantuml，https://plantuml.com/zh/download
(setq plantuml-default-exec-mode 'jar)
(setq plantuml-jar-path "~/.emacs.d/support/plantuml-1.2022.1.jar")

;; (setq org-plantuml-jar-path
;;       (expand-file-name "e:/downloads/plantuml-nodot.1.2022.1.jar"))

;; ;; Enable plantuml-mode for PlantUML files
;; (add-to-list 'auto-mode-alist '("\\.plantuml\\'" . plantuml-mode))

;; ;; Integration with org-mode
;; (add-to-list 'org-src-lang-modes '("plantuml" . plantuml))

(message "珍惜当下……")

;; windows操作系统使用git-bash运行emacs
;; 新建文本文件，内容如下（git-bash路径和emacs路径根据实际情况调整）
;; "E:\Program Files\Git\git-bash.exe" --command=E:/programs/Emacs/x86_64/bin/runemacs.exe
;; 保存为runemacs.cmd（随便什么名字，后缀为.cmd即可）
;; 然后就可以双击运行了
;; 这样就可以使用部分linux命令了
dddddddddd